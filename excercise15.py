# python excercise 15

# openeing a file in reading mode
file = open("file1.txt","r")

print("file name :- ",file.name)
print("file mode :- ",file.mode)
print("reading the file data :- ")
print(file.read())

# opening file twice

file = open("file1.txt","w")
print("writing into the file this will overrode all data available into the file ")
str ="I am writing into the file"
file.write(str)

print("return value :- ", file.isatty)
file.close()

